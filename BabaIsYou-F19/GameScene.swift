//
//  GameScene.swift
//  BabaIsYou-F19
//
//  Created by Parrot on 2019-10-17.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var isblock:SKSpriteNode!
    var isblock1:SKSpriteNode!
    var stopblock:SKSpriteNode!
    var flagblock:SKSpriteNode!
    var wallblock:SKSpriteNode!
    var wall:SKSpriteNode!
    var winblock:SKSpriteNode!
    var baba:SKSpriteNode!
    var flag:SKSpriteNode!
    let PLAYER_SPEED:CGFloat = 20

    override func didMove(to view: SKView) {
        self.physicsWorld.contactDelegate = self
        
        // setup isBlock sprite
            self.isblock = self.childNode(withName: "isblock") as! SKSpriteNode
            self.isblock.physicsBody = SKPhysicsBody(rectangleOf: self.isblock.size)
            self.isblock.physicsBody?.affectedByGravity = false
            self.isblock.physicsBody?.categoryBitMask = 1
            self.isblock.physicsBody?.collisionBitMask = 0
            self.isblock.physicsBody?.contactTestBitMask = 255
            self.isblock.physicsBody?.allowsRotation = false
        
        // setup isBlock1 sprite
                   self.isblock1 = self.childNode(withName: "isblock1") as! SKSpriteNode
                   self.isblock1.physicsBody = SKPhysicsBody(rectangleOf: self.isblock1.size)
                   self.isblock1.physicsBody?.affectedByGravity = false
                   self.isblock1.physicsBody?.categoryBitMask = 1
                   self.isblock1.physicsBody?.collisionBitMask = 0
                   self.isblock1.physicsBody?.contactTestBitMask = 255
                   self.isblock1.physicsBody?.allowsRotation = false
        
        
        // setup stopBlock sprite
              self.stopblock = self.childNode(withName: "stopblock") as! SKSpriteNode
              
              self.stopblock.physicsBody = SKPhysicsBody(rectangleOf: stopblock.size)
              self.stopblock.physicsBody?.affectedByGravity = false
              self.stopblock.physicsBody?.categoryBitMask = 2
              self.stopblock.physicsBody?.collisionBitMask = 255
              self.stopblock.physicsBody?.contactTestBitMask = 175
              self.stopblock.physicsBody?.allowsRotation = false
        
        // setup flagBlock sprite
        self.flagblock = self.childNode(withName: "flagblock") as! SKSpriteNode
        
        self.flagblock.physicsBody = SKPhysicsBody(rectangleOf: flagblock.size)
        self.flagblock.physicsBody?.affectedByGravity = false
        self.flagblock.physicsBody?.categoryBitMask = 4
        self.flagblock.physicsBody?.collisionBitMask = 255
        self.flagblock.physicsBody?.contactTestBitMask = 175
        self.flagblock.physicsBody?.allowsRotation = false
        
        // setup wallBlock sprite
        self.wallblock = self.childNode(withName: "wallblock") as! SKSpriteNode
        
        self.wallblock.physicsBody = SKPhysicsBody(rectangleOf: wallblock.size)
        self.wallblock.physicsBody?.affectedByGravity = false
        self.wallblock.physicsBody?.isDynamic = true
        self.wallblock.physicsBody?.categoryBitMask = 8
        self.wallblock.physicsBody?.collisionBitMask = 255
        self.wallblock.physicsBody?.contactTestBitMask = 175
        self.wallblock.physicsBody?.allowsRotation = false
        
        // setup wall sprites
             self.enumerateChildNodes(withName: "wall") {
                 (node, stop) in
                self.wall = node as! SKSpriteNode
                self.wall.physicsBody = SKPhysicsBody(rectangleOf: self.wall.size)
                 self.wall.physicsBody?.affectedByGravity = false
                self.wall.physicsBody?.categoryBitMask = 16
                self.wall.physicsBody?.collisionBitMask = 0
                 self.wall.physicsBody?.isDynamic = false
             }
        
        // setup winBlock sprite
              self.winblock = self.childNode(withName: "winblock") as! SKSpriteNode
              
              self.winblock.physicsBody = SKPhysicsBody(rectangleOf: winblock.size)
              self.winblock.physicsBody?.affectedByGravity = false
              self.winblock.physicsBody?.categoryBitMask = 32
              self.winblock.physicsBody?.collisionBitMask = 255
              self.winblock.physicsBody?.contactTestBitMask = 175
              self.winblock.physicsBody?.allowsRotation = false
        
        // setup baba sprite
                     self.baba = self.childNode(withName: "baba") as! SKSpriteNode
                     
                     self.baba.physicsBody = SKPhysicsBody(rectangleOf: baba.size)
                     self.baba.physicsBody?.affectedByGravity = false
                     self.baba.physicsBody?.categoryBitMask = 64
                     self.baba.physicsBody?.collisionBitMask = 127
                     self.baba.physicsBody?.contactTestBitMask = 255
                     self.baba.physicsBody?.allowsRotation = false
        
        // setup flag sprite
                     self.flag = self.childNode(withName: "flag") as! SKSpriteNode
                     
                     self.flag.physicsBody = SKPhysicsBody(rectangleOf: flag.size)
                     self.flag.physicsBody?.affectedByGravity = false
                     self.flag.physicsBody?.categoryBitMask = 128
                     self.flag.physicsBody?.collisionBitMask = 0
                     self.flag.physicsBody?.contactTestBitMask = 175
                     self.flag.physicsBody?.allowsRotation = false
        
        

    }
   
    func didBegin(_ contact: SKPhysicsContact) {


    }
    
    override func update(_ currentTime: TimeInterval) {
        
        
        //-----------------stop rules for (isblock)
        
         // baba cannot cross the walls
        if( Int(wallblock.position.x) - Int(isblock.position.x) == -64 && Int(isblock.position.x) - Int(stopblock.position.x) == -64 &&
                  (Int(wallblock.position.y) < Int(isblock.position.y)+5) && (Int(wallblock.position.y) > Int(isblock.position.y)-5) &&
                  (Int(stopblock.position.y) < Int(isblock.position.y)+5) && (Int(stopblock.position.y) > Int(isblock.position.y)-5)
                  
                  )
              {
                 // baba cannot cross the walls as wall is stop rule is active
                 print("cannot cross")
                  self.baba.physicsBody?.collisionBitMask = 127
              }

       // baba can  cross the walls
          else
          {
            // baba can cross the walls as wall is stop rule is not active
            print("can cross")
            self.stopblock.physicsBody?.collisionBitMask = 239
            self.winblock.physicsBody?.collisionBitMask = 239
            self.flagblock.physicsBody?.collisionBitMask = 239
            self.wallblock.physicsBody?.collisionBitMask = 239
                  

              self.baba.physicsBody?.collisionBitMask = 111
            
            
            
            // win rules
            // baba win by touching the flag
                  if( Int(flagblock.position.x) - Int(isblock1.position.x) == -64 && Int(isblock1.position.x) - Int(winblock.position.x) == -64 &&
                                   (Int(flagblock.position.y) < Int(isblock1.position.y)+5) && (Int(flagblock.position.y) > Int(isblock1.position.y)-5) &&
                                   (Int(winblock.position.y) < Int(isblock1.position.y)+5) && (Int(winblock.position.y) > Int(isblock1.position.y)-5)
                                   
                                   )
                               {
                                   // check whether the baba is touching the flag
                                  if(baba.frame.intersects(flag.frame) == true){
                                      print("congratulations you won")
                                  }
                                  
                               }
            
              // baba win by touching the wall
              if( Int(wallblock.position.x) - Int(isblock1.position.x) == -64 && Int(isblock1.position.x) - Int(winblock.position.x) == -64 &&
                  (Int(wallblock.position.y) < Int(isblock1.position.y)+5) && (Int(wallblock.position.y) > Int(isblock1.position.y)-5) &&
                  (Int(winblock.position.y) < Int(isblock1.position.y)+5) && (Int(winblock.position.y) > Int(isblock1.position.y)-5)
                  
                  )
              {
                  
                 // print("Congratulations you won")
                  
                  self.stopblock.physicsBody?.collisionBitMask = 239
                  self.winblock.physicsBody?.collisionBitMask = 239
                  self.flagblock.physicsBody?.collisionBitMask = 239
                  self.wallblock.physicsBody?.collisionBitMask = 239
                 if(baba.frame.intersects(wall.frame) == true){
                     print("congratulations you won")
                 }
                
              }

           
          }

        // flag can stop the baba
        
        print(Int(wallblock.position.x))
        print(Int(isblock.position.x))
        print(Int(stopblock.position.x))
        if( Int(flagblock.position.x) - Int(isblock.position.x) == -64 && Int(isblock.position.x) - Int(stopblock.position.x) == -64 &&
                                (Int(flagblock.position.y) < Int(isblock.position.y)+5) && (Int(flagblock.position.y) > Int(isblock.position.y)-5) &&
                                (Int(stopblock.position.y) < Int(isblock.position.y)+5) && (Int(stopblock.position.y) > Int(isblock.position.y)-5)
                                )
                            {
                            self.baba.physicsBody?.collisionBitMask = 239
                              
                               }
                               
    }
    
      override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
         // GET THE POSITION WHERE THE MOUSE WAS CLICKED
         // ---------------------------------------------
         let mouseTouch = touches.first
         if (mouseTouch == nil) {
             return
         }
         let location = mouseTouch!.location(in: self)

         // WHAT NODE DID THE PLAYER TOUCH
         // ----------------------------------------------
         let nodeTouched = atPoint(location).name
         //print("Player touched: \(nodeTouched)")
         
         
         // GAME LOGIC: Move player based on touch
         if (nodeTouched == "upButton") {
             // move up
            self.baba.position.y = self.baba.position.y + PLAYER_SPEED
         }
         else if (nodeTouched == "downButton") {
             // move down
              self.baba.position.y = self.baba.position.y - PLAYER_SPEED
         }
         else if (nodeTouched == "leftButton") {
             // move left
              self.baba.position.x = self.baba.position.x - PLAYER_SPEED
         }
         else if (nodeTouched == "rightButton") {
             // move right
              self.baba.position.x = self.baba.position.x + PLAYER_SPEED
         }
         
     }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
}
